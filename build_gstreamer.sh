#!/bin/sh

SUPPORTED_LIBS="gstreamer gst-plugins-base gst-plugins-good gst-plugins-bad gst-plugins-ugly gst-editing-services"

if [ -z "$1" ]; then
	LIBS=$SUPPORTED_LIBS
else
	if [ "$1" = "uninstall" ]; then
		UNINSTALL=1
		LIBS=$SUPPORTED_LIBS
	else
		LIBS=$1
		if [ ! -d "$1" ]; then
		echo "this folder '$1' does not exist. Exit."
		exit 1
		fi
	fi
fi

for LIB in $LIBS; do
	mkdir -p $LIB/build_dir
	cd $LIB/build_dir
	if [ -z $UNINSTALL ]; then
		[[ -f build.ninja ]] && RECONFIGURE="--reconfigure"
		meson $RECONFIGURE .. -Ddisable_gtkdoc=true
		#meson configure -Ddisable_gtkdoc=true
		ninja
		sudo ninja install
	else
		sudo ninja uninstall
	fi
	cd ../..
done

exit 0
